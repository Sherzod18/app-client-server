import api from 'api'
import {config} from 'utils';
// import {getDistricts, getGenders, getRegions, userMe, getBusinessType, deleteEdu, uploadFile,login} from 'services';
import router from 'umi/router';
import {toast} from "react-toastify";
import {routerRedux} from "dva";
// import {eduList} from "../pages/cabinet/edu/service";

const {
  getPositions, getAllFile
} = api;

function pagination(page, numPages) {
  var res = [];
  var from = 1;
  var to = numPages;
  if (numPages > 10) {
    from = Math.max(page - 2, 1);
    to = Math.max(Math.min(page + 2, numPages), 5);
    if (to > 5) {
      res.push(1);
      if (from > 2) res.push(2);
      if (from > 3) {
        if (from === 4) {
          res.push(3);
        } else {
          res.push("...");
        }
      }
    }
  }
  for (var i = from; i <= to; i++) {
    res.push(i);
  }

  if (numPages > 10) {
    if (to < (numPages - 2)) {
      if (to === 8) {
        res.push(9);
      } else {
        res.push("...");
      }
    }
    if (to < numPages)
      res.push(numPages - 1);
    if (to !== numPages)
      res.push(numPages);
  }
  return res;
}

export default {
  namespace: 'app',
  state: {
    isAdmin: false,
    isSort: false,
    photoId: '',
    eventBannerId: '',
    positions: [],
    regions: [],
    awares: [],
    speakers: [],
    currentUser: {},
    payTypes: [],
    cashTypes: [],
    pathname: '',
    users: [],
    isModalShow: false,
    isModalPlaceShow: false,
    logosId: [],
    vouchers: [],
    isLoading: false,
    loading: true,
    speakerSize: 1,
    balance: 0,
    currentItem: {},
    languages: [],
    totalPages: 0,
    page: 0,
    size: 10,
    paginations: [],
    search: '',
    expenseTypes: [],
    files:[]
  },
  subscriptions: {
    setupHistory({dispatch, history}) {
      history.listen((location) => {
        dispatch({
          type: 'updateState',
          payload: {
            pathname: location.pathname,
          },
        });
        // dispatch({
        //   type: 'userMe',
        //   payload: {
        //     pathname: location.pathname
        //   }
        // })
      })
    },
    setup({dispatch, history}) {
      history.listen(({pathname}) => {
        if (!config.openPages.includes(pathname) && !pathname.includes('/article/') && !pathname.includes('/videos/')) {
          dispatch({
            type: 'userMe',
            payload: {
              pathname
            }
          })
        }
        dispatch({
          type: 'userMe2',
          payload: {
            pathname
          }
        })
      })
    }
  },
  effects: {
    * getAllFile({payload}, {call, put, select}) {
      const res = yield call(getAllFile);
      console.log(res);
      // yield put({
      //   type: 'updateState',
      //   payload: {
      //     files: res._embedded.list
      //   }
      // })
    },
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
}
