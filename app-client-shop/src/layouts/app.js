import React, {Component} from 'react';
import 'antd/dist/antd.css';
import {Link} from "umi";
// import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
// import 'bootstrap/dist/css/bootstrap.css';
// import 'react-toastify/dist/ReactToastify.css';

import { Layout, Menu, Breadcrumb } from 'antd';
import {
  UserOutlined,
  LaptopOutlined,
  NotificationOutlined,
  MenuUnfoldOutlined,
  MenuFoldOutlined
} from '@ant-design/icons';
import {connect} from "dva";
import {config} from 'utils'

const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;
const {openPages, userPages} = config;

@connect(({app}) => ({app}))
class App extends Component {
  state = {
    collapsed: false,
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };
  render() {
    const {app, dispatch} = this.props;
    const {currentUser, pathname} = app;
    if (pathname === '/login') {
      return this.props.children
    }

    return (
      <Layout>
        <Header  className="header header1 sticky-top2 one-edge-shadow" >
          <div className="menu-block">
            <img src={require("../assets/rrr.png")} width={90} height={70} alt="" className="mr-4"/>
            {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
              className: 'trigger',
              onClick: this.toggle,
            })}
          </div>
          <div className="user-block push">
            <ul>
              <li><a href="#" className="mr-3">UZ</a></li>
              <li className="drop-down">
                <a href="#" className="d-flex">
                  <img src={require("../assets/avatar.jpg")} className="user-avatar" alt=""/>
                  <h6>Utaganov Sherzod</h6>
                  <b className="icon icon-caret-down"></b>
                </a>
                <div className="drop-down-menu">
                  <a href="#" className="drop-item"><b className="icon icon-setting"></b> Sittings </a>
                  <a href="#" className="drop-item"><b className="icon icon-logout"></b> Logout </a>
                </div>
              </li>
            </ul>
          </div>
        </Header>
        <Layout>
          <Sider  trigger={null} collapsible collapsed={this.state.collapsed} width={200} className="right-shadow">
            <div className="card-user" style={this.state.collapsed ? {display:"none"} : {display:"inline-block"}}>
              <img src={require("../assets/avatar.jpg")} className="user-avatar" alt=""/>
              <h6 className="mt-2">Utaganov Sherzod</h6>
              <p>admin, User, Manager</p>
            </div>
            <Menu
              mode="inline"
              // defaultSelectedKeys={['1']}
              defaultOpenKeys={['sub0']}
              style={this.state.collapsed ? {marginTop:"15px"} : {marginTop:"0"}}
            >
              <Menu.Item key="sub0">
                <b className="icon icon-manager mr-4"></b>Manager
              </Menu.Item>
              <Menu.Item key="sub1">
                <b className="icon icon-clock mr-4"></b>nav 2
              </Menu.Item>
              <Menu.Item key="sub2">
                <b className="icon icon-clock mr-4"></b>nav 2
              </Menu.Item>
              <Menu.Item key="sub3">
                <b className="icon icon-clock mr-4"></b>nav 2
              </Menu.Item>
              <SubMenu key="sub4"  title="subnav 1">
                <Menu.Item key="1">option1</Menu.Item>
                <Menu.Item key="2">option2</Menu.Item>
                <Menu.Item key="3">option3</Menu.Item>
                <Menu.Item key="4">option4</Menu.Item>
              </SubMenu>
              <SubMenu key="sub5" icon={<LaptopOutlined />} title="subnav 2">
                <Menu.Item key="5">option5</Menu.Item>
                <Menu.Item key="6">option6</Menu.Item>
                <Menu.Item key="7">option7</Menu.Item>
                <Menu.Item key="8">option8</Menu.Item>
              </SubMenu>
              <SubMenu key="sub6" icon={<NotificationOutlined />} title="subnav 3">
                <Menu.Item key="9">option9</Menu.Item>
                <Menu.Item key="10">option10</Menu.Item>
                <Menu.Item key="11">option11</Menu.Item>
                <Menu.Item key="12">option12</Menu.Item>
              </SubMenu>
            </Menu>
          </Sider>
          <Layout style={{ padding: '0 24px 24px' }}>
            <Breadcrumb style={{ margin: '30px 20px' }}>
              <Breadcrumb.Item>Home</Breadcrumb.Item>
              <Breadcrumb.Item>List</Breadcrumb.Item>
              <Breadcrumb.Item>App</Breadcrumb.Item>
            </Breadcrumb>
            <Content
              className="site-layout-background"

              style={{
                overflow: 'initial',
                padding: 60,
                margin: '0 20px',
                minHeight: 700,
              }}
            >
              {this.props.children}
            </Content>
          </Layout>
        </Layout>
      </Layout>
    );
  }
}

export default App;


