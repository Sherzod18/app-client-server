import 'antd/dist/antd.css';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import 'bootstrap/dist/css/bootstrap.css';
import 'react-toastify/dist/ReactToastify.css';
import '../global.scss';
import  '@ant-design/icons';
import {Fragment} from "react";
import {ToastContainer} from 'react-toastify';
import App from './app';

function BasicLayout(props) {
  return (
    <Fragment>
      <App>
        <ToastContainer/>
        {props.children}
      </App>
    </Fragment>
  );
}

export default BasicLayout;
