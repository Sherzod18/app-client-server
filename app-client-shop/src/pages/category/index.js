import React, {Component} from 'react';
import {connect} from "dva";
import {Button, Form, Input, Upload} from "antd";
import Modal from "./modal";
import PlusOutlined from "@ant-design/icons/lib/icons/PlusOutlined";

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

@connect(({category}) => ({category}))
class Category extends Component {


  componentDidMount(){
    const {dispatch} = this.props;
  }



  render() {

    const uploadButton = (
      <div>
        <PlusOutlined />
        <div className="ant-upload-text">Upload</div>
      </div>
    );
    const {category, dispatch} = this.props;
    const {isModalShow,confirmLoading, previewVisible, previewImage, fileList, previewTitle} = category;

    const modalProps = {
      visible:isModalShow,
      confirmLoading:confirmLoading,
      onOk(data){
        console.log(data);
        dispatch({
          type:"category/updateMyState",
          payload:{
            confirmLoading:true
          }
        });
        setTimeout(() => {
          dispatch({
            type:"category/updateMyState",
            payload:{
              isModalShow:false,
              confirmLoading:false
            }
          });
        }, 2000);
      },
      onCancel(){
        dispatch({
          type:"category/updateMyState",
          payload:{
            isModalShow:false
          }
        })
      },
    };


    const handlePreview = async file => {
      if (!file.url && !file.preview) {
        file.preview = await getBase64(file.originFileObj);
      }
      dispatch({
        type:"category/updateMyState",
        payload:{
          previewImage: file.url || file.preview,
          previewVisible: true,
          previewTitle: file.name || file.url.substring(file.url.lastIndexOf('/') + 1),
        }
      });
    };
    const handleChange = ({ fileList }) => {
      dispatch({
        type:"category/updateMyState",
        payload:{
          fileList
        }
      });

    };
    const modalShow = () => {
      dispatch({
        type:"category/updateMyState",
        payload:{
          isModalShow:true
        }
      })
    };

    return (
      <div>
        <div className="clearfix">
          <Upload
            action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
            listType="picture-card"
            fileList={fileList}
            onPreview={handlePreview}
            onChange={handleChange}
          >
            {fileList.length >= 3 ? null : uploadButton}
          </Upload>
        </div>
        <h1>Category</h1>
        <b className="icon icon-clock"></b>
        <Button type="primary" className="my-2" onClick={modalShow}>Add Category</Button>
        {isModalShow && <Modal {...modalProps}/>}
      </div>
    );
  }
}

export default Category;
