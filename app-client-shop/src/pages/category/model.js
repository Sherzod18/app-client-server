import api from 'api'
import {config} from 'utils';
// import {getDistricts, getGenders, getRegions, userMe, getBusinessType, deleteEdu, uploadFile,login} from 'services';
import router from 'umi/router';
import {toast} from "react-toastify";
import {routerRedux} from "dva";
// import {eduList} from "../pages/cabinet/edu/service";

const {
   getAllFile
} = api;

export default {
  namespace: 'category',
  state: {
    isAdmin: false,
    currentUser: {},
    pathname: '',
    users: [],
    isModalShow: false,
    isModalPlaceShow: false,
    isLoading: false,
    confirmLoading: false,
    loading: true,
    files:[],
    previewVisible: false,
    previewImage: '',
    previewTitle: '',
    fileList: [

    ],
  },
  subscriptions: {
    setupHistory({dispatch, history}) {
      history.listen((location) => {
        dispatch({
          type: 'updateMyState',
          payload: {
            pathname: location.pathname,
          },
        });
        // dispatch({
        //   type: 'userMe',
        //   payload: {
        //     pathname: location.pathname
        //   }
        // })
      })
    },
    setup({dispatch, history}) {
      history.listen(({pathname}) => {
        if (!config.openPages.includes(pathname) && !pathname.includes('/article/') && !pathname.includes('/videos/')) {
          dispatch({
            type: 'userMe',
            payload: {
              pathname
            }
          })
        }
        dispatch({
          type: 'userMe2',
          payload: {
            pathname
          }
        })
      })
    }
  },
  effects: {
    * getAllFile({payload}, {call, put, select}) {
      const res = yield call(getAllFile);
      console.log(res);
      // yield put({
      //   type: 'updateMyState',
      //   payload: {
      //     files: res._embedded.list
      //   }
      // })
    },
  },
  reducers: {
    updateMyState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
}
