import React from "react";
import PropTypes from "prop-types";
import {Form, Input, Modal, Upload} from "antd";
import { PlusOutlined } from '@ant-design/icons';

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

const categoryModal =({
                        form:{getFieldDecorator, validateFields, getFieldsValue},
                        onOk,
                      ...modalProps}) => {

  const handleOk =()=> {
    validateFields((error) => {
      if (error){
        return
      }
      let formData = getFieldsValue();
      // console.log(formData);
      onOk(formData);
    });
  };

  const customProps = {
    ...modalProps,
    onOk:handleOk,
  };
  const uploadButton = (
    <div>
      <PlusOutlined />
      <div className="ant-upload-text">Upload</div>
    </div>
  );
  const dummyRequest =({file, onSuccess}) =>{
    console.log(file);
    setTimeout(()=>{
      onSuccess("ok");
    }, 0)
  };

  const normFile =(e)=>{
    if (Array.isArray(e)){
      return e;
    }
    if (e.fileList.length > 1){
      e.fileList.shift();
    }
    if (e && e.fileList){
      // console.log(e);
      // console.log(e.fileList);
      return e && e.fileList;
    }

  };
  return (
    <Modal {...customProps}>
      <Form>
        <Form.Item label="First name" key="firstName">
          {getFieldDecorator("firstName", {
            rules: [{required : true, message: 'Please input your First name'}]
          })(<Input placeholder="First name"/>)}
        </Form.Item>
        <Form.Item label="Last name" key="lastName">
          {getFieldDecorator("lastName", {
            rules: [{required : true, message: 'Please input your First name'}]
          })( <Input placeholder="Last name"/>)}
        </Form.Item>
        <Form.Item label="Phone number" key="phoneNumber">
          {getFieldDecorator("phoneNumber", {
            rules: [{required : true, message: 'Please input your First name'}]
          })(<Input placeholder="Phone number"/>)}
        </Form.Item>
        <Form.Item label="File" key="file">
          {getFieldDecorator("file",{
            valuePropName:"fileList",
            getValueFromEvent: normFile,
            rules: [{required : true, message: 'Please input your File'}]
          })(
            <Upload  action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                     listType="picture-card" name="file" customRequest={dummyRequest} >
              {uploadButton}
            </Upload>
          )}
        </Form.Item>
      </Form>
    </Modal>
  );
};

categoryModal.propTypes = {
  form: PropTypes.object.isRequired,
  type: PropTypes.string,
  item: PropTypes.object,
  onSubmit: PropTypes.func,
};

export default Form.create()(categoryModal)

