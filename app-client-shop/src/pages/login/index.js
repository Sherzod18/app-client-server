import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Col, Container, Row, Spinner} from "reactstrap";
import MaskedInput from "react-text-mask";
import {connect} from "dva";
import router from "umi/router";
import section from "eslint-plugin-jsx-a11y/lib/util/implicitRoles/section";
import {Toast as toast} from "react-toastify";

@connect(({auth}) => ({auth}))
class Login extends Component {

  componentDidMount() {
    const {dispatch, auth} = this.props;
    dispatch({
      type: 'auth/updateState',
      payload: {
        phoneNumber: '',
        isVerifyProcess: false,
        hasRegistered: false,
        checkPhoneProcess: true,
      }
    });



  }

  componentWillMount() {
    const {dispatch, auth} = this.props;
    const {currentUser} = auth;
    if (localStorage.getItem('token')) {
      dispatch({
        type: 'auth/userMe'
      }).then(res => {
        if (res.success) {
          if (res.object.roles.filter(i => i.name === 'ROLE_ADMIN').length > 0) {
            router.push('/cabinet');
          }

          if (res.object.roles.length === 1 && res.object.roles.filter(i => i.name === "ROLE_USER")) {
            router.push('/auth/' + res.object.id)
          }
        } else {
          localStorage.removeItem('token')
        }
      });
    }
  }

  render() {
    const {auth, dispatch} = this.props;
    const {
      checkPhoneProcess, hasRegistered, phoneNumber, password, isLoading,
      reCaptcha, intervalId, isVerifyProcess, confirmationResult,
      code, firstName, lastName, confirmPassword, codeTime, hasPassword, prePassword, buttonVisible
    } = auth;
    const login = (e) => {
      e.preventDefault();
      if ((!hasRegistered && confirmPassword === password)) {
        dispatch({
          type: 'auth/checkUser',
        }).then(res => {
          if (res.success) {

          }
        })
      } else if (hasRegistered && hasPassword) {
        dispatch({
          type: 'auth/checkUser',
        }).then(res => {
          if (res.success) {

          }
        })
      } else if (hasRegistered && !hasPassword && prePassword === password) {

      } else {
        toast.error("Parollar bir xil emas!");
      }
    };
    const updatePhone = (e) => {

      dispatch({
        type: 'auth/updateState',
        payload: {
          [e.target.name]: e.target.value,
          isLoading: false
        }
      });
      dispatch({
        type: 'auth/checkState'
      })
    };
    const updateState = (e) => {
      console.log(e.target.name + " =  " +e.target.value);
      dispatch({
        type: 'auth/updateState',
        payload: {
          [e.target.name]: e.target.value
        }
      })
    };
    return (
      <section className="bg-b">
        <Container id="login_page" className="login_page">
          <Row className="mt-5">
            <Col md={{size:4, offset:4}}>
              <h1>Salom</h1>
              <form onSubmit={login}>
                <div className="wrap">
                  <label htmlFor="login">Telefon raqam</label>
                  <div className="input-group">
                    <div className="input-group-prepend">
                      <b className="icon icon-user"></b>
                    </div>
                    {/*<AvField name="phoneNumber"/>*/}
                    <MaskedInput
                      placeholder="+998"
                      className="form-control"
                      id="login"
                      name="phoneNumber"
                      onChange={updatePhone}
                      mask={["+", "9", "9", "8", /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]}
                    />
                  </div>
                </div>
                {!checkPhoneProcess && hasRegistered && hasPassword ?
                  <div className="wrap mt-3 animated-label">
                    <div className="input-group">
                      <div className="input-group-prepend">
                        <b className="icon icon-key"></b>
                      </div>
                      {/*<AvField name="password" type="password"/>*/}
                      <input type="password"
                             id="password"
                             name="password"
                             className="form-control"
                             onChange={updateState} required/>
                      <label htmlFor="password">Parol</label>
                    </div>
                  </div> : ''}
                {!checkPhoneProcess && hasRegistered && !hasPassword ?
                  <div>
                    <div className="wrap mt-3 animated-label">
                      <div className="input-group">
                        <div className="input-group-prepend">
                          <b className="icon icon-key"></b>
                        </div>
                        {/*<AvField name="password" type="password"/>*/}
                        <input type="password"
                               id="password"
                               name="password"
                               className="form-control"
                               onChange={updateState} required/>
                        <label htmlFor="password">Parol</label>
                      </div>
                    </div>
                    <div className="wrap mt-3 animated-label">
                      <div className="input-group">
                        <div className="input-group-prepend">
                          <b className="icon icon-key"></b>
                        </div>
                        {/*<AvField name="password" type="password"/>*/}
                        <input type="password"
                               id="pre-password"
                               name="prePassword"
                               className="form-control"
                               onChange={updateState} required/>
                        <label htmlFor="password">Parolni takrorlang</label>
                      </div>
                    </div>
                  </div> : ''
                }
                {!checkPhoneProcess && !hasRegistered ?
                  <div>
                    <div className="wrap mt-3 animated-label">
                      <div className="input-group">
                        <div className="input-group-prepend">
                          <span className="icon icon-user"/>
                        </div>
                        <input type="text"
                               onChange={updateState}
                               id="name" name="firstName"
                               className="form-control" required/>
                        <label htmlFor="name">Ism</label>
                      </div>
                    </div>
                    <div className="wrap mt-3 animated-label">
                      <div className="input-group">
                        <div className="input-group-prepend">
                          <span className="icon icon-user"/>
                        </div>
                        <input type="text"
                               onChange={updateState}
                               name="lastName" id="company"
                               className="form-control" required/>
                        <label htmlFor="company">Familiya</label>
                      </div>
                    </div>
                    <div className="wrap mt-3 animated-label">
                      <div className="input-group">
                        <div className="input-group-prepend">
                          <span className="icon icon-key"/>
                        </div>
                        <input type="password" onChange={updateState}
                               name="password" id="password"
                               className="form-control" required/>
                        <label htmlFor="password">Parol</label>
                      </div>
                    </div>
                    <div className="wrap mt-3 animated-label">
                      <div className="input-group">
                        <div className="input-group-prepend">
                          <span className="icon icon-key"/>
                        </div>
                        <input type="password"
                               onChange={updateState}
                               name="confirmPassword" id="confirmPassword"
                               className="form-control" required/>
                        <label htmlFor="confirmPassword">Takroriy parol</label>
                      </div>
                    </div>
                  </div>

                  : ''}
                {isLoading ?
                  <div style={{margin: "35px auto", width: "35px"}}><Spinner/></div> :
                  !checkPhoneProcess && buttonVisible ? <div className="col-md-12 entrance-login">
                    <button type="submit"
                            className="btn btn-danger font-weight-bold mt-5 py-2 px-5">Kirish
                    </button>
                  </div> : ""
                }
              </form>
            </Col>
          </Row>
        </Container>
      </section>
    );
  }
}

Login.propTypes = {};

export default Login;
