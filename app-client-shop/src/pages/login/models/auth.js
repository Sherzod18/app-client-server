import api from 'api'
import {routerRedux} from 'dva'
import {parsePhoneNumber} from 'libphonenumber-js'
import {STORAGE_NAME} from 'utils/constant';
import {router} from 'umi';

const {userCheckPhone, signUp, userMe, signIn, checkUser, setPassword} = api;

export default ({
  namespace: 'auth',
  state: {
    phoneNumber: '',
    password: '',
    checkPhoneProcess: true,
    hasRegistered: false,
    reCaptcha: null,
    intervalId: '',
    confirmationResult: null,
    isVerifyProcess: false,
    code: '',
    firstName: '',
    lastName: '',
    confirmPassword: '',
    hasPassword: true,
    prePassword: '',
    isLoading: false,
    buttonVisible: false,
    currentUser: {}
  },
  subscriptions: {
    setup({dispatch, history}) {
      history.listen((location) => {
        if (location.pathname === '/login') {

        }
      });
    }
  },
  effects: {
    * userMe({payload}, {call, put, select}) {
      const res = yield call(userMe);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            currentUser: res.object
          }
        })
      }
      return res;
    },

    * checkPhone({payload}, {put, call, select}) {
      const data = yield call(userCheckPhone, payload);
      if (data.success) {
        yield put({
          type: 'updateState',
          payload: {
            checkPhoneProcess: false,
            hasRegistered: data.hasRegistered,
            hasPassword: data.hasPassword,
            isLoading: false,
            buttonVisible: true

          },
        });

        yield put(routerRedux.push('/login'))
      }
    },
    * checkState({payload}, {put, call, select}) {
      const {phoneNumber} = yield select(_ => _.auth);
      try {
        if (parsePhoneNumber(phoneNumber).isValid()) {
          yield put({
            type: 'updateState',
            payload: {
              isLoading: true
            }
          });
          yield put({
            type: 'checkPhone',
            payload: {
              phoneNumber
            }
          })
        } else {
          yield put({
            type: 'updateState',
            payload: {
              checkPhoneProcess: true,
              hasRegistered: false
            },
          });
        }
      } catch (e) {
        yield put({
          type: 'updateState',
          payload: {
            checkPhoneProcess: true,
            hasRegistered: false
          },
        });
      }
    },
    * cancelPhone({payload}, {put, call, select}) {
      yield put({
        type: 'updateState',
        payload: {
          phoneNumber: '',
          isVerifyProcess: false,
          buttonVisible: false
        },
      });
      // yield put(routerRedux.push('/start'))
    },
    // eslint-disable-next-line no-empty-pattern
    * checkUser({}, {put, call, select}) {
      const {password, phoneNumber} = yield select(_ => _.auth);
      return yield call(checkUser, {phoneNumber, password});
    },
    * sign({payload}, {put, call, select}) {
      const {hasRegistered} = yield select(_ => _.auth);
      yield put({
        type: "updateState",
        payload: {
          isLoading: true
        }
      });
      if (!payload.hasPassword && hasRegistered) {
        let setPasswordBody = {
          phoneNumber: payload.phoneNumber,
          password: payload.password,
          prePassword: payload.prePassword
        }
        const res = yield call(setPassword, setPasswordBody)
        if (res.success) {
          yield put({
            type: 'updateState',
            payload: {
              hasPassword: true
            }
          })
          yield put({
            type: "signInOrSignUp",
            payload: {
              payload
            }
          })
        }
      } else {
        yield put({
          type: "signInOrSignUp",
          payload: {
            payload
          }
        })
      }

    },
    * signInOrSignUp({payload}, {put, call, select}) {
      var reqBody = {};
      if (payload.payload.hasRegistered) {
        reqBody = {
          phoneNumber: payload.payload.phoneNumber,
          password: payload.payload.password
        }
      } else {
        reqBody = payload.payload
      }

      const data = yield call(payload.payload.hasRegistered ? signIn : signUp, reqBody);

      yield put({
        type: 'updateState',
        payload: {
          verificationCode: ''
        },
      });

      if (data.body.accessToken !== undefined) {
        localStorage.setItem(STORAGE_NAME, data.body.tokenType + " " + data.body.accessToken);
        const res = yield put({
          type: 'userMe'
        });
        res.then(res => {
          if (res.object.roles.filter(i => i.name === 'ROLE_ADMIN').length > 0) {
            router.push('/cabinet')
          }

          if (res.object.roles.length === 1 && res.object.roles.filter(i => i.name === "ROLE_USER")) {
            router.push('/user/' + res.object.id)
          }
        })

      } else {
        yield put({
          type: 'updateState',
          payload: {
            isLoading: false
          },
        });
      }
    },

  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
