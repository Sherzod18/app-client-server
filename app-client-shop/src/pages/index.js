import React, {Component} from 'react';
import {connect} from 'dva';

@connect(({app}) => ({app}))
class Index extends Component {

  componentDidMount(){
    const {dispatch} = this.props;
    dispatch({
      type: 'app/getAllFile'
    });
  }

  render() {
    return (
      <div>
        <h1>Content</h1>
      </div>
    );
  }
}

export default Index;
