export default {

  signUp: 'POST /auth/register',
  signIn: 'POST /auth/login',

  userCheckPhone: 'POST /auth/checkPhone',
  checkUser: 'POST /auth/checkUser',
  userMe: '/auth/me',
  register: 'POST /user',
  searchUser: 'POST /auth/search',
  getUsers: '/user',
  getUsersBySort: '/auth/byCountVisit',
  getUser: '/user',
  getByPhone: '/auth/getByPhone',
  editUser: 'PUT /user',
  editPassword: 'PUT /auth/changePassword',
  setPassword: 'PUT /auth/setPasword',
  getUserIncomesAndOutcomes: '/auth/incomeAndOutcome',
  getAllFile: '/file/all',

  uploadFile: 'POST /file',

  getEvents: '/event',
  getEventsForUser: '/event/forUser',
  getArchiveAndCancelEvents: '/event/archiveAndCancel',
  getEventsByType: '/event/type',
  getEvent: '/event',
  searchEvent: '/event/search',
  getUpcomingEventsOfUser: '/event/user-event',
  getParticipatedEvents: '/event/user-archive-event',
  getUpcomingEvents: '/event/upcoming',
  getEventForEdit: '/event/get',
  getOpenEvents: '/event/open',
  addEvent: 'POST /event',
  editEvent: 'PUT /event',
  eventIncome: '/event/income',
  getEventUsers: '/event/users',
  getParticipatedAndGuest: '/event/participate',
  changeStatusEventOpen: 'PUT /event/open',
  getEventStatuses: ' /event/status',
  getSeatPaymentsByEvent: '/event/seatPayment',
  getDashboard: '/event/dashboard',

  addMessage: 'POST /message',
  getMessages: '/message',


  getOwnReview: '/review',
  getReview: '/review/all',
  addReview: 'POST /review',
  getMainPageReview: '/review',
  getAllReviews: '/review/list',
  addMainPageReview: 'POST /review/save',
  editMainPageReview: 'PATCH /review/save',
  changePublished: 'PUT /review/published',
  getReviewTotalPages: '/review/totalPages',

  addPayment: 'POST /payment',
  getPayments: '/payment',
  getTemplatePlaces: '/place',
  saveTemplatePlace: 'POST /place',
  deleteTemplate: 'DELETE /place',


  seatOrder: 'POST /seat',
  bookSeat: 'POST /seat/book',
  cancelBooked: 'PUT /seat/cancelBooked',
  addVip: 'PUT /seat/addVip',
  addReserve: 'PUT /seat/addReserve',
  participate: 'PUT /seat/participate',
  cancelSold: 'PUT /seat/cancelSold',

  addSpeaker: 'POST /speaker',
  editSpeaker: 'PUT /speaker',
  getSpeakers: '/speaker',
  getSpeaker: '/speaker',
  getSpeakersPagination: '/speaker/pagination',
  deleteSpeaker: 'DELETE /speaker',

  getVoucher: '/voucher',
  getVouchers: '/voucher',
  addVoucher: 'POST /voucher',
  editVoucher: 'PUT /voucher',
  deleteVoucher: 'DELETE /voucher',

  getSeats: '/seat',
  orderSeat: 'POST /seat',
  addChairr: 'POST /seat/addChair',
  addRow: 'POST /seat/addRow',

  addActivity: 'POST /activity',
  editActivity: 'PUT /activity',
  getActivities: '/activity',
  getActivity: '/activity',
  deleteActivity: 'DELETE /activity',

  addAware: 'POST /aware',
  editAware: 'PUT /aware',
  getAwares: '/aware',
  getAware: '/aware',
  deleteAware: 'DELETE /aware',

  addEventType: 'POST /eventType',
  editEventType: 'PUT /eventType',
  getEventTypes: '/eventType',
  getEventType: '/eventType',
  deleteEventType: 'DELETE /eventType',

  addPayType: 'POST /payType',
  getPayTypes: '/payType',
  getPayType: '/payType',
  editPayType: 'PUT /payType',
  deletePayType: 'DELETE /payType',

  addCashType: 'POST /cashType',
  getCashTypes: '/cashType',
  getCashType: '/cashType',
  editCashType: 'PUT /cashType',
  deleteCashType: 'DELETE /cashType',
  addTransfer: 'POST /cashType/transfer',

  addLanguage: 'POST /language',
  getLanguages: '/language',
  getLanguage: '/language',
  editLanguage: 'PUT /language',
  deleteLanguage: 'DELETE /language',

  addPosition: 'POST /position',
  editPosition: 'PUT /position',
  getPositions: '/position',
  getPosition: '/position',
  deletePosition: 'DELETE /position',

  addAttachmentType: 'POST /attachmentType',
  editAttachmentType: 'PUT /attachmentType',
  getAttachmentTypes: '/attachmentType',
  getAttachmentType: '/attachmentType',
  deleteAttachmentType: 'DELETE /attachmentType',


  addRegion: 'POST /region',
  editRegion: 'PUT /region',
  getRegions: '/region',
  getRegion: '/region',
  deleteRegion: 'DELETE /region',

  addTariff: 'POST /tariff',
  editTariff: 'PATCH /tariff',
  getTariffs: '/tariff',
  getTariff: '/tariff',
  deleteTariff: 'DELETE /tariff',

  getRoles: '/role',
  addTicket: 'POST /ticket',

  addExpenseType: 'POST /expenseType',
  getExpenseTypes: '/expenseType',
  getExpenseType: '/expenseType',
  editExpenseType: 'PUT /expenseType',
  deleteExpenseType: 'DELETE /expenseType',

  getExpenses: '/expense',
  getExpense: '/expense',
  addExpense: 'POST /expense',
  editExpense: 'PUT /expense',
  deleteExpense: 'PUT /expense',

  getCategory: '/category',
  getCategories: '/category',
  addCategory: 'POST /category',
  editCategory: 'PATCH /category',
  deleteCategory: 'DELETE /category',

  savePost: 'POST /article',
  getArticles: '/article',
  getTotalPages: '/article/totalPages',
  deleteArticle: 'DELETE /article',

  searchArticle: '/article/search',
  getArticlesByActive: '/article/active',
  getArticlesByCategory: '/article/byCategory',
  articleByUrl: '/article/byUrl',

  getLatest3Articles: '/article/latest3',
  getAllVideos: '/video',
  saveVideo: 'POST /video',
  deleteVideo: 'DELETE /video',
  lastVideos: '/video/lasVideos',
  videoTypes: '/video/getVideoTypes',
  getPraktikums: '/video/getPraktikums',
  searchByTitle: '/video/searchByTitle',

  anonsType: '/video/anonsType',


  getArchives: '/archive',

  getFaqs: '/faq?sort=createdAt,desc',
  updateFaq: 'PATCH /faq',
  deleteFaq: 'DELETE /faq',
  createFaq: 'POST /faq',

  addVideoType: 'POST /videoType',
  editVideoType: 'PUT /videoType',
  getVideoTypes: '/videoType',
  getVideoType: '/videoType',
  deleteVideoType: 'DELETE /videoType',

  getComments: '/comment',
  addComment: 'POST /comment',

  addUsd: 'POST /waitingExpense/usd',
  editUsd: 'PUT /waitingExpense/usd',
  getUsd: '/waitingExpense/usd',

  addWaitingExpense: 'POST /waitingExpense',
  editWaitingExpense: 'PUT /waitingExpense',
  getWaitingExpenses: '/waitingExpense',
  deleteWaitingExpense: 'DELETE /waitingExpense',


  exportExcel: '/event/exportExcel',
  debt: '/waitingExpense/debtAndDebt',

}

