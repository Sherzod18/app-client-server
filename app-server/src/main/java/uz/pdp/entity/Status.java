package uz.pdp.entity;

import lombok.Data;
import uz.pdp.entity.enums.UserStatus;

import javax.persistence.*;


@Data
@Entity
public class Status {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private UserStatus type;
}
