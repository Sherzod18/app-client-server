package uz.pdp.entity.enums;

public enum UserStatus {
    REGISTERED,
    ACTIVE,
    BLOCKED
}
