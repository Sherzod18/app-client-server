package uz.pdp.entity.enums;

public enum FileStorageStatus {
    ACTIVE, DRAFT
}
