package uz.pdp.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.entity.FileStorage;
import uz.pdp.entity.enums.FileStorageStatus;

import java.util.List;

@Repository
public interface FileStorageRepository extends JpaRepository<FileStorage, Long> {
    FileStorage findByHashId(String hashId);
    List<FileStorage> findAllByFileStorageStatus(FileStorageStatus fileStorageStatus);
}
