package uz.pdp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.entity.Role;
import uz.pdp.entity.enums.RoleName;

import java.util.List;
import java.util.Optional;


public interface RoleRepository extends JpaRepository<Role, Integer> {
    List<Role> findByRoleName(RoleName name);

    List<Role> findAllByRoleName(RoleName name);

//    Optional<Role> findByRoleName(RoleName name);

    List<Role> findAllByRoleNameIn(List<RoleName> names);
}
