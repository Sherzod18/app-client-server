package uz.pdp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.entity.CategoryI18n;

@Repository
public interface CategoryI18nRepository extends JpaRepository<CategoryI18n, Integer> {
}
