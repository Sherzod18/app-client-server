package uz.pdp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.entity.Status;
import uz.pdp.entity.enums.UserStatus;

@Repository
public interface StatusRepository extends JpaRepository<Status, Integer> {
    Status findByType(UserStatus type);
}
