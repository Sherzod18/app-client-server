package uz.pdp.commonent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.pdp.entity.User;
import uz.pdp.entity.enums.RoleName;
import uz.pdp.entity.enums.UserStatus;
import uz.pdp.repository.RoleRepository;
import uz.pdp.repository.StatusRepository;
import uz.pdp.repository.UserRepository;


import java.util.Arrays;

@Component
public class DataLoader implements CommandLineRunner {

    @Value("${spring.datasource.initialization-mode}")
    private String initialMode;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    StatusRepository statusRepository;


    @Override
    public void run(String... args) throws Exception {
        if (initialMode.equals("always")) {
            User user = new User("+998993631856", passwordEncoder.encode("root123"),
                    "Sherzod", "Utaganov",
                    statusRepository.findByType(UserStatus.REGISTERED),
                    roleRepository.findAllByRoleNameIn(Arrays.asList(
                            RoleName.ROLE_USER,
                            RoleName.ROLE_ADMIN,
                            RoleName.ROLE_MANAGER
                    )));
            userRepository.save(user);
            System.out.println("DataLoader ishladi");
        }
    }
}
