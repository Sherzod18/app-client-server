package uz.pdp.service;

import org.hashids.Hashids;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.pdp.entity.FileStorage;
import uz.pdp.entity.enums.FileStorageStatus;
import uz.pdp.payload.ApiResponseModel;
import uz.pdp.payload.ResUploadFile;
import uz.pdp.repository.FileStorageRepository;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Service
public class FileStorageService {
    private final FileStorageRepository fileStorageRepository;

    @Value("${upload.folder}")
    private String uploadFolder;

    private final Hashids hashids;

    public FileStorageService(FileStorageRepository fileStorageRepository) {
        this.fileStorageRepository = fileStorageRepository;
        this.hashids = new Hashids(getClass().getName(), 6);
    }

    public ApiResponseModel uploadSave(MultipartHttpServletRequest request) throws IOException {
        Iterator<String> iterator = request.getFileNames();
        MultipartFile file;
        List<ResUploadFile> resUploadFiles = new ArrayList<>();
        while (iterator.hasNext()){
            file = request.getFile(iterator.next());
            FileStorage fileStorage = new FileStorage();
            assert file != null;
            fileStorage.setName(file.getOriginalFilename());
            fileStorage.setExtension(getExt(file.getOriginalFilename()));
            fileStorage.setFileSize(file.getSize());
            fileStorage.setContentType(file.getContentType());
            fileStorage.setFileStorageStatus(FileStorageStatus.DRAFT);
            fileStorageRepository.save(fileStorage);

            Date now = new Date();

            File uploadFolder = new File(String.format("%s/upload_files/%d/%d/%d/", this.uploadFolder,
                    1900 + now.getYear(), 1 + now.getMonth(), now.getDate()));

            if (!uploadFolder.exists() && uploadFolder.mkdirs()){
                System.out.println("aytilgan papkalar yaratildi!");
            }

            fileStorage.setHashId(hashids.encode(fileStorage.getId()));
            fileStorage.setUploadPath(String.format("upload_files/%d/%d/%d/%s.%s",
                    1900 + now.getYear(), 1 + now.getMonth(), now.getDate(),
                    fileStorage.getHashId(),
                    fileStorage.getExtension()));
            fileStorageRepository.save(fileStorage);

            uploadFolder = uploadFolder.getAbsoluteFile();

            File file1 = new File(uploadFolder, String.format("%s.%s", fileStorage.getHashId(), fileStorage.getExtension()));

            try {
                file.transferTo(file1);
            }catch (IOException e){
                e.printStackTrace();
            }

            resUploadFiles.add(new ResUploadFile(
                    fileStorage.getId(),
                    fileStorage.getName(),
                    null,
                    fileStorage.getContentType(),
                    fileStorage.getFileSize()
            ));
        }
        return new ApiResponseModel(true, "", resUploadFiles);
    }

    @Transactional(readOnly = true) // faqat uqib olish uchun
    public FileStorage findByHashId(String hashIsh){
        return fileStorageRepository.findByHashId(hashIsh);
    }

    public void delete(String hashId){
        FileStorage fileStorage = findByHashId(hashId);
        File file = new File(String.format("%s/%s", this.uploadFolder, fileStorage.getUploadPath()));
        if (file.delete()){
            fileStorageRepository.delete(fileStorage);
        }
    }

    @Scheduled(cron = "0 0 0 * * *") // har kuni soat 00:00 da ishlaydi
    public void deleteAllDraft(){
        List<FileStorage> fileStorages = fileStorageRepository.findAllByFileStorageStatus(FileStorageStatus.DRAFT);
//        for (FileStorage fileStorage: fileStorages){
//            delete(fileStorage.getHashId());
//        }

        fileStorages.forEach(fileStorage -> {
            delete(fileStorage.getHashId());
        });

    }

    private String getExt(String fileName){
        String ext = null;
        if (fileName != null && !fileName.isEmpty()){
            int dot = fileName.lastIndexOf('.');
            if (dot > 0 && dot <= fileName.length()-2){
                ext = fileName.substring(dot+1);
            }
        }
        return ext;
    }
}
