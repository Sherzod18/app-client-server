package uz.pdp.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.pdp.payload.ReqAuthLogin;
import uz.pdp.repository.CategoryRepository;
import uz.pdp.service.FileStorageService;

import java.io.IOException;

@RestController
@RequestMapping("/api/category")
public class CategoryController {

    private final FileStorageService fileStorageService;

    private final CategoryRepository categoryRepository;

    public CategoryController(FileStorageService fileStorageService, CategoryRepository categoryRepository) {
        this.fileStorageService = fileStorageService;
        this.categoryRepository = categoryRepository;
    }

    @PostMapping("/created")
    public ResponseEntity<?> createdCategory(MultipartHttpServletRequest request) throws IOException {
        fileStorageService.uploadSave(request);
        return ResponseEntity.ok("yaratildi");
    }

}
