package uz.pdp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.entity.Language;
import uz.pdp.payload.ApiResponseModel;
import uz.pdp.repository.LanguageRepository;

import java.util.List;

@RestController
@RequestMapping("/api/language")
public class LanguageController {

    @Autowired
    LanguageRepository languageRepository;

    @PostMapping("/add")
    public ResponseEntity<?> addLanguage(@RequestParam String name, @RequestParam String locale){
        Language language = new Language();
        language.setName(name);
        language.setLocale(locale);
        languageRepository.save(language);
        return ResponseEntity.ok("til yaratildi");
    }

    @GetMapping("/all")
    public ApiResponseModel getAllLanguage(){
        List<Language> all = languageRepository.findAll();
        if (all.size() > 0){
            return new ApiResponseModel(true, "", all);
        }
        return new ApiResponseModel(false, "Tilni yarating", all);
    }

}
