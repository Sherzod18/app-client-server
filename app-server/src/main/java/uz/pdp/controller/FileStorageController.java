package uz.pdp.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileUrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.pdp.entity.FileStorage;
import uz.pdp.payload.ApiResponseModel;
import uz.pdp.repository.FileStorageRepository;
import uz.pdp.service.FileStorageService;

import java.io.IOException;
import java.net.URLEncoder;

@RestController
@RequestMapping("/api/file")
public class FileStorageController {

    private final FileStorageService fileStorageService;

    private final FileStorageRepository fileStorageRepository;

    @Value("${upload.folder}")
    private String uploadFolder;

    public FileStorageController(FileStorageService fileStorageService, FileStorageRepository fileStorageRepository) {
        this.fileStorageService = fileStorageService;
        this.fileStorageRepository = fileStorageRepository;
    }

    @PostMapping("/upload")
    public ApiResponseModel uploadFileStorage(MultipartHttpServletRequest request) throws IOException {
        return fileStorageService.uploadSave(request);
    }

    @GetMapping("/all")
    public ApiResponseModel getAllFile(){
        return new ApiResponseModel(true, "success", fileStorageRepository.findAll());
    }

    @GetMapping("/preview/{hashId}")
    public ResponseEntity<?> previewFile(@PathVariable String hashId) throws IOException{
        FileStorage fileStorage = fileStorageService.findByHashId(hashId);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "inline: fileName=\"" + URLEncoder.encode(fileStorage.getName()))
                .contentType(MediaType.parseMediaType(fileStorage.getContentType()))
                .contentLength(fileStorage.getFileSize())
                .body(new FileUrlResource(String.format("%s/%s", uploadFolder, fileStorage.getUploadPath())));
    }

    @GetMapping("/download/{hashId}")
    public ResponseEntity<?> downloadFile(@PathVariable String hashId) throws IOException{
        FileStorage fileStorage = fileStorageService.findByHashId(hashId);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment: fileName=\"" + URLEncoder.encode(fileStorage.getName()))
                .contentType(MediaType.parseMediaType(fileStorage.getContentType()))
                .contentLength(fileStorage.getFileSize())
                .body(new FileUrlResource(String.format("%s/%s", uploadFolder, fileStorage.getUploadPath())));
    }

    @DeleteMapping("/delete/{hashId}")
    public ResponseEntity<?> deleteFile(@PathVariable String hashId){
        fileStorageService.delete(hashId);
        return ResponseEntity.ok("file o'chirildi!");
    }
}
