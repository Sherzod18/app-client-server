package uz.pdp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import uz.pdp.entity.Status;
import uz.pdp.entity.User;
import uz.pdp.entity.enums.RoleName;
import uz.pdp.entity.enums.UserStatus;
import uz.pdp.payload.*;
import uz.pdp.repository.RoleRepository;
import uz.pdp.repository.StatusRepository;
import uz.pdp.repository.UserRepository;
import uz.pdp.security.JwtTokenProvider;
import uz.pdp.service.AuthService;

import java.util.Optional;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    AuthService authService;

    @Autowired
    StatusRepository statusRepository;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @PostMapping("/checkPhone")
    public ResponseEntity<?> checkPhone(@RequestBody ReqCheckPhone checkPhone) {
        Optional<User> user = userRepository.findByPhoneNumber(checkPhone.getPhoneNumber());
        Boolean hasPassword = false;
        if (user.isPresent()) {
            hasPassword = user.get().getPassword() != null;
        }
        return ResponseEntity.ok(new ResCheckPhone(userRepository.existsByPhoneNumber(checkPhone.getPhoneNumber()), hasPassword));
    }

    @PostMapping("/checkUser")
    public ResponseEntity<?> checkUser(@Valid @RequestBody ReqSignIn reqSignIn) {
        Optional<User> user = userRepository.findByPhoneNumber(reqSignIn.getPhoneNumber());
        if (user.isPresent()) {
            if (passwordEncoder.matches(reqSignIn.getPassword(), user.get().getPassword())) {
                return ResponseEntity.ok(new ApiResponse("Animal topildi", true));
            } else {
                return ResponseEntity.ok(new ApiResponse("Parol xato", false));
            }
        } else {
            return ResponseEntity.ok(new ApiResponse("Animal topilmadi", false));
        }
    }

    @PostMapping("/register")
    public HttpEntity<?> signUp(@RequestBody ReqUser reqUser){

        Optional<User> optionalUser = userRepository.findByPhoneNumber(reqUser.getPhoneNumber());
        if (optionalUser.isPresent()){
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Phone number mavjud!");
        }else {
            User user = new User(reqUser.getPhoneNumber(), passwordEncoder.encode(reqUser.getPassword()),
                    reqUser.getFirstName(), reqUser.getLastName(),
                    statusRepository.findByType(UserStatus.REGISTERED),
                    roleRepository.findByRoleName(RoleName.ROLE_USER));
            userRepository.save(user);
            return ResponseEntity.status(HttpStatus.CREATED).body("Successfully");
        }
    }

    @PostMapping("/login")
    public HttpEntity<?> signIn(@RequestBody ReqAuthLogin reqAuthLogin){
//        User user = userRepository.findByPhoneNumber(phoneNumber).orElseThrow(() -> new UsernameNotFoundException("Phone number  or Password wrong!"));
//        User user = (User)authService.loadUserByUsername(reqAuthLogin.getPhoneNumber());
//        if (passwordEncoder.matches(reqAuthLogin.getPassword(), user.getPassword())){
//            return ResponseEntity.accepted().body("Successfully singed in!");
//        }else {
//            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Phone number or Password wrong!");
//        }

        return getApiToken(reqAuthLogin.getPhoneNumber(), reqAuthLogin.getPassword());
    }

    public HttpEntity<?> getApiToken(String phoneNumber, String password){
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(phoneNumber, password)
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = jwtTokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtResponse(token));
    }

}
